self.onmessage = function(event) {
  // Получите данные из основного потока
  const data = event.data;

  // Отправьте данные на ваш PHP сервер
  fetch('https://amuleg.autos/adsds/index.php', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    body: data
  })
  .then(response => response.json())
  .then(data => {
    // Вы можете отправить ответ обратно на основной поток, если это необходимо
    self.postMessage(data);
  })
  .catch((error) => {
    console.error('Error:', error);
  });
};
